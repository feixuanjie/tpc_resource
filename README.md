<h1 align="center">三方组件资源汇总</h1> 

##### 本文收集了一些已经开源的三方组件资源，欢迎鸿蒙开发者参考和使用，同时也欢迎开发者贡献自己的开源组件库，可以提PR加入到列表当中



## 目录

- [工具](#工具)
- [三方组件](#三方组件)
    - [工具类](#工具类)
        - [图片加载](#图片加载)
        - [数据封装传递](#数据封装传递)
        - [日志](#日志)
        - [权限相关](#权限相关)
        - [相机-相册](#相机-相册)
        - [其他工具类](#其他工具类)
    - [网络类](#网络类)
        - [网络类](#网络类)
    - [文件数据类](#文件数据类)
        - [数据库](#数据库)
        - [Preferences](#preferences)
        - [数据存储](#数据存储)
    - [UI-自定义控件](#ui-自定义控件)
        - [Image](#image)
        - [Text](#text)
        - [Button](#button)
        - [ListContainer](#listcontainer)
        - [PageSlider](#pageslider)
        - [ProgressBar](#progressbar)
        - [Dialog-弹出框](#dialog-弹出框)
        - [Layout](#layout)
        - [Tab-菜单切换](#tab-菜单切换)
        - [Time-Date](#time-date)
        - [其他UI-自定义控件](#其他ui-自定义控件)
    - [框架类](#框架类)
        - [框架类](#框架类)
    - [动画图形类](#动画图形类)
        - [动画](#动画)
        - [图片处理](#图片处理)
    - [音视频](#音视频)

## 工具

- [IDE官方下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio) - DevEco Studio
- [HAPM官网地址](https://hpm.harmonyos.com/hapm/#/cn/home) - [HAPM介绍](https://hpm.harmonyos.com/hapm/#/cn/help/introduction)

[返回目录](#目录)

## 三方组件

### 工具类

#### 图片加载

- [glide](https://gitee.com/openharmony-tpc/glide) - 最常用的图片加载工具
- [glide-transformations](https://gitee.com/openharmony-tpc/glide-transformations) - 基于glide 的图片变化库
- [fresco](https://gitee.com/openharmony-tpc/fresco) - facebook出品的一款图片加载工具
- [picasso](https://gitee.com/openharmony-tpc/picasso) - 常用的图片加载工具之一
- [ohos-gif-drawable](https://gitee.com/openharmony-tpc/ohos-gif-drawable) - gif图片加载工具
- [Keyframes](https://gitee.com/openharmony-tpc/Keyframes) - gif图片加载工具
- [ion](https://gitee.com/openharmony-tpc/ion) - 图片加载工具
- [coil](https://gitee.com/baijuncheng-open-source/coil) - 一款用于图片加载的库

[返回目录](#目录)

#### 数据封装传递

- [EventBus](https://gitee.com/openharmony-tpc/EventBus) - 最常用的消息传递工具，发布/订阅事件总线
- [Rxohos](https://gitee.com/openharmony-tpc/Rxohos) - RxJava3的openharmony特定绑定的反应性扩展。该模块向RxJava添加了最小的类，这些类使在openharmony应用程序中编写反应式组件变得容易且轻松。更具体地说，它提供了一个可在主线程或任何给定EventRunner上进行调度的Scheduler
- [RxBus](https://gitee.com/openharmony-tpc/RxBus) - 基于Rxjava消息传递工具
- [otto](https://gitee.com/openharmony-tpc/otto) - 基于Guava的消息传递工具
- [RxLifeCycle](https://gitee.com/openharmony-tpc/RxLifeCycle) - 基于RxJava生命周期获取，此功能很有用，因为不完整的订阅可能会导致内存泄漏
- [RxBinding](https://gitee.com/openharmony-tpc/RxBinding) - 用于ohos中UI小部件的RxJava绑定API
- [agera](https://gitee.com/openharmony-tpc/agera) - Agera 是一组类和接口，用于帮助编写功能性、异步和反应式应用程序
- [Anadea_RxBus](https://gitee.com/openharmony-tpc/Anadea_RxBus) - 基于Rxjava消息传递工具
- [LoadSir](https://gitee.com/openharmony-tpc/LoadSir) - 注册事件进行回调操作
- [Aria](https://gitee.com/hihopeorg/Aria) - 文件下载上传框架

[返回目录](#目录)

#### 日志

- [Logger](https://gitee.com/openharmony-tpc/logger) - log工具，简单，漂亮，功能强大的记录器	
- [xLog](https://gitee.com/openharmony-tpc/xLog) - 日志工具，可同时在多个通道打印日志，如 hilog、Console 和文件。如果你愿意，甚至可以打印到远程服务器（或其他任何地方）	
- [KLog](https://gitee.com/chinasoft_ohos/KLog) - HiLog 工具类	
- [tinylog_ohos](https://gitee.com/archermind-ti/tinylog_ohos) - 日志工具	
- [Timber_ohos](https://gitee.com/isrc_ohos/timber_ohos) - 基于开源项目Timber进行鸿蒙化的移植和开发，增强鸿蒙输出日志的能力

[返回目录](#目录)

#### 权限相关

- [XXPermissions](https://gitee.com/openharmony-tpc/XXPermissions) - 权限申请，一键式权限请求框架	
- [PermissionsDispatcher](https://gitee.com/openharmony-tpc/PermissionsDispatcher)	 - 权限申请，提供了一个简单的基于注解的API来处理运行时权限。该库减轻了编写一堆检查语句（无论是否已授予您权限）带来的负担，以保持您的代码干净安全	
- [Dexter](https://gitee.com/openharmony-tpc/Dexter) - 权限申请，简化在运行时请求权限的过程
- [RuntimePermission](https://gitee.com/archermind-ti/RuntimePermission) - 请求运行时权限的最简单方法，不需要扩展类或重写permissionResult方法	
- [permission-helper](https://gitee.com/baijuncheng-open-source/permission-helper) - 权限管理请求库

[返回目录](#目录)

#### 相机-相册

- [BGAQRCode-ohos](https://gitee.com/openharmony-tpc/BGAQRCode-ohos) - 基于ZXing的二维码扫描工具	
- [Matisse](https://gitee.com/openharmony-tpc/Matisse) - 相册访问	
- [ImagePicker](https://gitee.com/openharmony-tpc/ImagePicker) - 相册访问
- [CameraView](https://gitee.com/openharmony-tpc/CameraView) - 相机使用组件
- [Zxing](https://gitee.com/hihopeorg/Ohos-zxing) -二维码扫描器
- [easyqrlibrary](https://gitee.com/archermind-ti/easyqrlibrary) - 二维码扫描器
- [zxing-embedded](https://gitee.com/baijuncheng-open-source/zxing-embedded) - 基于ZXING，二维码条形码扫描库
- [qrcode-reader-view](https://gitee.com/baijuncheng-open-source/qrcode-reader-view) - 一个简易的相机扫码工具
- [barcodescanner](https://gitee.com/baijuncheng-open-source/barcodescanner) - 基于zxing和zbar提供易于使用的二维码扫描功能	
- [certificate-camera](https://gitee.com/baijuncheng-open-source/certificate-camera) - 一个拍摄证件照片的相机工具。	
- [Zbar_ohos](https://gitee.com/isrc_ohos/ZBar_ohos) - 基于开源项目Zbar进行鸿蒙化的移植和开发，条形码阅读	

[返回目录](#目录)

#### 其他工具类

- [Butterknife](https://gitee.com/openharmony-tpc/butterknife) - 通过反射调用方法，使用注解处理为您生成样板代码	
- [assertj-ohos](https://gitee.com/openharmony-tpc/assertj-ohos) - 一组旨在测试ohos的断言库
- [ohos-utilset](https://gitee.com/openharmony-tpc/ohos-utilset) - 工具集
- [xUtils3](https://gitee.com/openharmony-tpc/xUtils3) - 工具集 包含网络，图片，控件等	
- [device-year-class](https://gitee.com/openharmony-tpc/device-year-class) - 获取手机年份
- [swipe](https://gitee.com/openharmony-tpc/swipe) - 对于手势封装应用
- [TinyPinyin](https://gitee.com/openharmony-tpc/TinyPinyin) - 文字转拼音工具
- [OHOS_BluetoothKit](https://gitee.com/hihopeorg/Ohos-bluetooth-kit) - 蓝牙设备通信
- [Ohos-IMSI-Catcher-Detector](https://gitee.com/hihopeorg/Ohos-IMSI-Catcher-Detector) - IMSI探测器
- [Battery_Metrics](https://gitee.com/hihopeorg/Battery-Metrics) - 检测电池相关系统指标的库
- [CheckVersionLib](https://gitee.com/hihopeorg/CheckVersionLib) - 版本检测更新库
- [ErrorProne](https://gitee.com/hihopeorg/error-prone) - 将常见的Java语法错误捕获为编译错误显示出来	
- [FastBle](https://gitee.com/hihopeorg/FastBle) - 蓝牙设备通信
- [RxOhosBle](https://gitee.com/hihopeorg/rxohosble) - 蓝牙设备通信
- [truth](https://gitee.com/hihopeorg/truth) - 代码断言工具
- [KeyboardVisibilityEvent](https://gitee.com/hihopeorg/KeyboardVisibilityEvent) - 键盘显示隐藏监听工具
- [StatusBarUtil](https://gitee.com/hihopeorg/StatusBarUtil) - 状态栏管理工具
- [Router](https://gitee.com/chinasoft_ohos/Router) - 通过一行url去指定打开指定页面Ability的工具
- [Once](https://gitee.com/chinasoft_ohos/Once) - 提供一个简单的API来跟踪应用程序是否已经在给定的范围内执行了操作
- [libphonenumber-ohos](https://gitee.com/chinasoft_ohos/libphonenumber-ohos) - 电话归属地查询	
- [ohos-gesture-detectors](https://gitee.com/chinasoft_ohos/ohos-gesture-detectors) - 实现各种手势检测功能	
- [Commonmark-java](https://gitee.com/chinasoft_ohos/Commonmark-java) - 自定义表扩展名
- [DylanStepCount](https://gitee.com/chinasoft_ohos/DylanStepCount) - 计步工具
- [LocationManager](https://gitee.com/chinasoft_ohos/LocationManager) - 简化Android用户位置的获取
- [phrase](https://gitee.com/chinasoft_ohos/phrase) - 字符串处理工具
- [JsonLube](https://gitee.com/chinasoft_ohos/JsonLube) - Json高效解析工具
- [Notify-ohos](https://gitee.com/chinasoft_ohos/Notify-ohos) - 一个统一通知管理的功能库	
- [objenesis_ohos](https://gitee.com/archermind-ti/objenesis_ohos) - Objenesis是一个轻量级的Java库，作用是绕过构造器创建一个实例
- [update-checker-lib](https://gitee.com/baijuncheng-open-source/update-checker-lib) - 目前仅酷安网的更新检查检查
- [Parceler_ohos](https://gitee.com/isrc_ohos/parceler_ohos) - 序列化与反序列化封装实现
- [JodaTime_ohos](https://gitee.com/isrc_ohos/joda-time_ohos) - 日期和时间处理库
- [ANR-WatchDog-ohos](https://gitee.com/isrc_ohos/anr-watch-dog-ohos) - 检测ANR错误并引发有意义的异常工具
- [ViewServer_ohos](https://gitee.com/isrc_ohos/view-server_ohos) - 可视化界面显示布局调试支持工具	
- [libyuv](https://gitee.com/openharmony-tpc/libyuv) - 将ARGB图像转换为RGBA
- [ReLinker](https://gitee.com/openharmony-tpc/ReLinker) - native库加载器
- [FastBle](https://gitee.com/openharmony-tpc/FastBle) - 蓝牙快速开发框架
- [LoganSquare](https://gitee.com/openharmony-tpc/LoganSquare) - JSON解析和序列化库
- [CustomActivityOnCrash](https://gitee.com/openharmony-tpc/CustomActivityOnCrash) - 崩溃时启动自定义页面	
- [RxScreenshotDetector](https://gitee.com/openharmony-tpc/RxScreenshotDetector) - 截屏检测器
- [seismic](https://gitee.com/openharmony-tpc/seismic) - 设备抖动检测
- [AutoDispose](https://gitee.com/openharmony-tpc/AutoDispose) - RxJava工具库	
- [webp-ohos](https://gitee.com/openharmony-tpc/webp-ohos) - 节省内存空间的图片形式
- [Encryption](https://gitee.com/archermind-ti/Encryption) - 字符串加密解密工具
- [Ohos-Intent-Library](https://gitee.com/archermind-ti/Ohos-Intent-Library) - Intent跳转封装库
- [Armadillo](https://gitee.com/archermind-ti/armadillo) - 加密Preferences数据

[返回目录](#目录)

### 网络类

#### 网络类

- [PersistentCookieJar](https://gitee.com/openharmony-tpc/PersistentCookieJar) - 基于okhttp3实现的cookie网络优化
- [chuck](https://gitee.com/openharmony-tpc/chuck) - okhttp本地client
- [google-http-java-client](https://gitee.com/openharmony-tpc/google-http-java-client) - google http Client库
- [ohos-async-http](https://gitee.com/openharmony-tpc/ohos-async-http) - 基于Apache的HttpClient库构建的Http Client
- [okhttp-OkGo](https://gitee.com/openharmony-tpc/okhttp-OkGo) - 基于okhttp 封装的库
- [ohosAsync](https://gitee.com/openharmony-tpc/ohosAsync) - 异步网络请求
- [Fast-ohos-Networking](https://gitee.com/openharmony-tpc/Fast-ohos-Networking) - 快速访问
- [FileDownloader](https://gitee.com/openharmony-tpc/FileDownloader) - 文件下载库
- [PRDownloader](https://gitee.com/openharmony-tpc/PRDownloader) - 文件下载库
- [network-connection-class](https://gitee.com/openharmony-tpc/network-connection-class) - 获取网络状态库
- [ThinDownloadManager](https://gitee.com/openharmony-tpc/ThinDownloadManager) - 文件下载库
- [AndServer](https://gitee.com/hihopeorg/AndServer) - 网络部署与反向代理设置
- [autobahn-java](https://gitee.com/hihopeorg/autobahn-java) - WebSocket协议和Web应用程序消息传递协
- [paho.mqtt.embedded-c](https://gitee.com/hihopeorg/paho.mqtt.embedded-c) - MQTT协议客户端
- [Smack](https://gitee.com/hihopeorg/Smack) - 用于与XMPP服务器进行通信，以执行实时通信，包括即时消息和群聊
- [RxEasyHttp](https://gitee.com/hihopeorg/RxEasyHttp) - 基于RxJava2+Retrofit2实现简单易用的网络请求框架
- [retrofit-cache_ohos](https://gitee.com/archermind-ti/retrofit-cache_ohos) - 通过注解配置，可以针对每一个接口灵活配置缓存策略
- [hdc-join-wifi](https://gitee.com/baijuncheng-open-source/hdc-join-wifi) - 连接wifi热点的样例
- [okdownload](https://gitee.com/openharmony-tpc/okdownload) - 下载引擎
- [NoHttp](https://gitee.com/openharmony-tpc/NoHttp) - 网络请求
- [ReactiveNetwork](https://gitee.com/openharmony-tpc/ReactiveNetwork) - 监听网络连接状态以及与RxJava Observables的Internet连接
- [okhttputils](https://gitee.com/openharmony-tpc/okhttputils) - okhttp的封装辅助工具

[返回目录](#目录)

### 文件数据类

#### 数据库

- [greenDAO](https://gitee.com/openharmony-tpc/greenDAO) - 最常用的数据库组件
- [Activeohos](https://gitee.com/openharmony-tpc/Activeohos) - 数据库sqlite封装
- [RushOrm](https://gitee.com/openharmony-tpc/RushOrm) - 数据库sqlite封装
- [LitePal](https://gitee.com/openharmony-tpc/LitePal) - 数据库sqlite封装
- [debug-database](https://gitee.com/baijuncheng-open-source/debug-database) - 封装原生数据库的增删改查操作， ORM方式操作对象对应数据库中的数据
- [ohos-database-sqlcipher](https://gitee.com/openharmony-tpc/ohos-database-sqlcipher) - 数据库加密
- [ohos-NoSql](https://gitee.com/openharmony-tpc/ohos-NoSql) - 轻量数据库
- [ormlite-ohos](https://gitee.com/openharmony-tpc/ormlite-ohos) - 数据库

[返回目录](#目录)

#### Preferences

- [rx-preferences](https://gitee.com/openharmony-tpc/rx-preferences) - 基于Preferences封装存储工具
- [preferencebinder](https://gitee.com/openharmony-tpc/preferencebinder) - 基于Preferences封装存储工具
- [PreferenceRoom](https://gitee.com/chinasoft_ohos/PreferenceRoom) - 一个高效且结构化管理Preference的功能库
- [tray](https://gitee.com/baijuncheng-open-source/tray) - Preference 替代库

[返回目录](#目录)

#### 数据存储

- [DiskLruCache](https://gitee.com/hihopeorg/DiskLruCache) - 磁盘Lru存储
- [MMKV](https://gitee.com/hihopeorg/MMKV) - 数据持久化键值对存储
- [hawk](https://gitee.com/openharmony-tpc/hawk) - 安全，简单的键值存储
- [tray](https://gitee.com/openharmony-tpc/tray) - 跨进程数据管理方法
- [Parceler](https://gitee.com/openharmony-tpc/Parceler) - 任何类型的数据传输

[返回目录](#目录)


### UI-自定义控件

#### Image

- [PhotoView](https://gitee.com/openharmony-tpc/PhotoView) - 图片缩放查看
- [CircleImageView](https://gitee.com/openharmony-tpc/CircleImageView) - 圆形图片
- [RoundedImageView](https://gitee.com/openharmony-tpc/RoundedImageView) - 圆角图片
- [subsampling-scale-image-view](https://gitee.com/openharmony-tpc/subsampling-scale-image-view) - 超高清图查看缩放
- [ContinuousScrollableImageView](https://gitee.com/openharmony-tpc/ContinuousScrollableImageView) - 带动画播放的Image
- [AvatarImageView](https://gitee.com/openharmony-tpc/AvatarImageView) - 头像显示库
- [DraggableView](https://gitee.com/openharmony-tpc/DraggableView) - 自定义可拖拽Image

[返回目录](#目录)

#### Text

- [drawee-text-view](https://gitee.com/openharmony-tpc/drawee-text-view) - 富文本组件
- [ReadMoreTextView](https://gitee.com/openharmony-tpc/ReadMoreTextView) - 点击展开的Text控件
- [MaterialEditText](https://gitee.com/openharmony-tpc/MaterialEditText) - 基于MaterialDesign设计的自定义输入框
- [XEditText](https://gitee.com/openharmony-tpc/XEditText) - 自定义特殊效果输入
- [lygttpod_SuperTextView](https://gitee.com/hihopeorg/lygttpod_SuperTextView) - 各种样式的自定义Text控件
- [TagView](https://gitee.com/chinasoft_ohos/TagView) - 实现文本可操作标签
- [BankCardFormat](https://gitee.com/chinasoft_ohos/BankCardFormat) - 自定义银行卡号输入框
- [AutoVerticalTextview](https://gitee.com/chinasoft_ohos/AutoVerticalTextview) - 纵向自动滚动的text
- [RTextView](https://gitee.com/chinasoft_ohos/RTextView) - 自定义Text控件，支持多种形状效果
- [JustifiedTextView](https://gitee.com/chinasoft_ohos/JustifiedTextView) - 文本对齐的Text控件
- [TextBannerView](https://gitee.com/openharmony-tpc/TextBannerView) - 文字轮播图
- [ohos-viewbadger](https://gitee.com/openharmony-tpc/ohos-viewbadger) - 文本标签View
- [ticker](https://gitee.com/openharmony-tpc/ticker) - 显示滚动文本
- [stefanjauker_BadgeView](https://gitee.com/openharmony-tpc/stefanjauker_BadgeView) - 仿iOS Springboard
- [CountAnimationTextView](https://gitee.com/openharmony-tpc/CountAnimationTextView) - Text动画计数

[返回目录](#目录)

#### Button

- [FloatingActionButton](https://gitee.com/openharmony-tpc/FloatingActionButton) - 悬浮button
- [circular-progress-button](https://gitee.com/openharmony-tpc/circular-progress-button) - 自定义带进度的按钮
- [progressbutton](https://gitee.com/openharmony-tpc/progressbutton) - 带进度的自定义按钮
- [SwitchButton](https://gitee.com/openharmony-tpc/SwitchButton) - 仿ios的开关按钮
- [SlideSwitch](https://gitee.com/openharmony-tpc/SlideSwitch) - 多种样式的开关按钮
- [iOS-SwitchView](https://gitee.com/chinasoft_ohos/iOS-SwitchView) - 仿ios的开关按钮
- [Highlight](https://gitee.com/openharmony-tpc/Highlight) - 指向性功能高亮
- [SwitchButton](https://gitee.com/openharmony-tpc/SwitchButton) - 开关按钮
- [slideview](https://gitee.com/openharmony-tpc/slideview) - 自定义滑动按钮

[返回目录](#目录)

#### ListContainer

- [FloatingGroupExpandableListView](https://gitee.com/openharmony-tpc/FloatingGroupExpandableListView) - 自定义list组件，支持分类带标题
- [XRecyclerView](https://gitee.com/openharmony-tpc/XRecyclerView) - 基于ListContainer下拉刷新
- [PullToZoomInListView](https://gitee.com/openharmony-tpc/PullToZoomInListView) - 顶部放大List
- [WaveSideBar](https://gitee.com/openharmony-tpc/WaveSideBar) - 类似于通讯录带字母选择的list
- [SwipeActionAdapter](https://gitee.com/openharmony-tpc/SwipeActionAdapter) - list侧滑菜单
- [ToDoList](https://gitee.com/hihopeorg/ToDoList) - 支持多样性自定义化的list控件
- [SectionedRecyclerViewAdapter](https://gitee.com/hihopeorg/SectionedRecyclerViewAdapter) - 支持多样性自定义化的list控件
- [ARecyclerView](https://gitee.com/chinasoft_ohos/ARecyclerView) - 自定义listContainer控件
- [StickyHeadersib](https://gitee.com/chinasoft_ohos/StickyHeaders) - 支持列表分组标题
- [RoundedLetterView](https://gitee.com/chinasoft_ohos/RoundedLetterView) - 简单的通讯录ui库
- [AStickyHeader_ohos](https://gitee.com/isrc_ohos/asticky-header_ohos) - 分组标题栏滑动时置顶效果
- [CalendarListview](https://gitee.com/openharmony-tpc/CalendarListview) - 日历选择器
- [SlideAndDragListView](https://gitee.com/openharmony-tpc/SlideAndDragListView) - 自定义ListContaner控件
- [pinned-section-listview](https://gitee.com/openharmony-tpc/pinned-section-listview) - 支持列表分组标题
- [HeaderAndFooterRecyclerView](https://gitee.com/openharmony-tpc/HeaderAndFooterRecyclerView) - 支持addHeaderView，addFooterView到ListContainer
- [MultiType](https://gitee.com/openharmony-tpc/MultiType) - 为简便ListContainer创建多种类型
- [StickyListHeaders](https://gitee.com/openharmony-tpc/StickyListHeaders) - 支持列表分组标题

[返回目录](#目录)

#### PageSlider

- [ViewPagerIndicator](https://gitee.com/openharmony-tpc/ViewPagerIndicator) - 星级最高的Slider组件
- [PageIndicatorView](https://gitee.com/openharmony-tpc/PageIndicatorView) - 自定义适配器组件
- [UltraViewPager](https://gitee.com/openharmony-tpc/UltraViewPager) - 多种样式的Slider自定义控件
- [SlidingDrawer](https://gitee.com/openharmony-tpc/SlidingDrawer) - 自定义Slider组件
- [AppIntro](https://gitee.com/openharmony-tpc/AppIntro) - 为应用程序构建一个很酷的轮播介绍
- [ParallaxViewPager](https://gitee.com/openharmony-tpc/ParallaxViewPager) - 自定义Slider组件
- [MZBannerView](https://gitee.com/openharmony-tpc/MZBannerView) - 仿魅族BannerView自定义组件
- [FlycoPageIndicator](https://gitee.com/openharmony-tpc/FlycoPageIndicator) - 多种样式的页面指示器
- [SCViewPager](https://gitee.com/openharmony-tpc/SCViewPager) - 具有转场动画的PageSlider自定义控件
- [imagecoverflow](https://gitee.com/openharmony-tpc/ImageCoverFlow) - 3D视角适配器
- [Ohos-ConvenientBanner](https://gitee.com/hihopeorg/Ohos-ConvenientBanner) - 自定义banner组件
- [DynamicPagerIndicator](https://gitee.com/chinasoft_ohos/DynamicPagerIndicator) - 仿爱奇艺/腾讯视频ViewPager导航条实现
- [Banner](https://gitee.com/chinasoft_ohos/Banner) - Banner图片轮播控件
- [Material-ViewPagerIndicator](https://gitee.com/chinasoft_ohos/Material-ViewPagerIndicator) - 页面指示器，实现平移，显隐组合动画效果
- [Banner_ohos](https://gitee.com/isrc_ohos/banner_ohos) - 广告图片轮播控件

[返回目录](#目录)

#### ProgressBar

- [MaterialProgressBar](https://gitee.com/openharmony-tpc/MaterialProgressBar) - 多种样式自定义progressbar
- [discreteSeekBar](https://gitee.com/openharmony-tpc/discreteSeekBar) - 冒泡式显示自定义seekbar
- [materialish-progress](https://gitee.com/openharmony-tpc/materialish-progress) - 自定义样式的progressbar
- [ohos-HoloCircularProgressBar](https://gitee.com/openharmony-tpc/ohos-HoloCircularProgressBar) - 自定义progressBar
- [circular-music-progressbar](https://gitee.com/openharmony-tpc/circular-music-progressbar) - 类似于音乐播放器的圆形progressbar
- [SectorProgressView](https://gitee.com/openharmony-tpc/SectorProgressView) - 自定义圆形progressBar
- [LikeSinaSportProgress](https://gitee.com/openharmony-tpc/LikeSinaSportProgress) - 仿新浪体育客户端的点赞进度条
- [ArcSeekBar](https://gitee.com/hihopeorg/ArcSeekBar) - 带有弧度的seekbar
- [MaterialishProgress](https://gitee.com/hihopeorg/materialish-progress) - Materia风格的Progress控件
- [RoundCornerProgressBar](https://gitee.com/hihopeorg/RoundCornerProgressBar) - 进度条效果设置库
- [BoxedVerticalSeekBar](https://gitee.com/chinasoft_ohos/BoxedVerticalSeekBar) - 自定义纵向seekbar
- [ProgressWheel_ohos](https://gitee.com/isrc_ohos/progress-wheel_ohos) - 开源进度轮
- [MagicProgressWidget](https://gitee.com/openharmony-tpc/MagicProgressWidget) - 颜色渐变的圆形进度条和纯色轻量横向进度条
- [NumberProgressBar](https://gitee.com/openharmony-tpc/NumberProgressBar) - 各种类型的progressBar的组合
- [ArcProgressStackView](https://gitee.com/openharmony-tpc/ArcProgressStackView) - 弧形模式下显示进度条
- [ProgressPieView](https://gitee.com/openharmony-tpc/ProgressPieView) - 自定义进度饼
- [CoreProgress](https://gitee.com/openharmony-tpc/CoreProgress) - 上传加载进度框架
- [CircularProgressView](https://gitee.com/openharmony-tpc/CircularProgressView) - Material圆形进度条
- [ButtonProgressBar](https://gitee.com/openharmony-tpc/ButtonProgressBar) - 自定义按钮进度条
- [ProgressView](https://gitee.com/openharmony-tpc/ProgressView) - 自定义ProgressView
- [CircleProgress](https://gitee.com/openharmony-tpc/CircleProgress) - 自定义圆形进度条
- [CProgressButton](https://gitee.com/openharmony-tpc/CProgressButton) - 自定义进度条按钮

[返回目录](#目录)

#### Dialog-弹出框

- [sweet-alert-dialog](https://gitee.com/openharmony-tpc/sweet-alert-dialog) - 自定义对话框
- [LovelyDialog](https://gitee.com/openharmony-tpc/LovelyDialog) - 自定义样式的Dialog，一组简单的对话框包装类库，旨在帮助您轻松创建精美对话框
- [CookieBar](https://gitee.com/openharmony-tpc/CookieBar) - 顶部底部弹出的自定义对话框
- [Alerter](https://gitee.com/openharmony-tpc/Alerter) - 顶部提示组件
- [StatusView](https://gitee.com/openharmony-tpc/StatusView) - 顶部弹出的状态视图
- [ohos-styled-dialogs](https://gitee.com/hihopeorg/ohos-styled-dialogs) - 自定义风格化Dialog
- [NiceDialog](https://gitee.com/chinasoft_ohos/NiceDialog) - NiceDialog基于CommonDialog的扩展，让dialog的使用更方便
- [BlurDialogFragment](https://gitee.com/baijuncheng-open-source/blur-dialog-fragment) - 模糊效果对话框
- [SnackBar_ohos](https://gitee.com/isrc_ohos/SnackBar_ohos) - 开源SnackBar消息弹框
- [michaelbel_BottomSheet](https://gitee.com/openharmony-tpc/michaelbel_BottomSheet) - material design弹框
- [search-dialog](https://gitee.com/openharmony-tpc/search-dialog) - 搜索Dialog
- [material-dialogs](https://gitee.com/openharmony-tpc/material-dialogs) - Material风格Dialog
- [BottomDialog](https://gitee.com/openharmony-tpc/BottomDialog) - 底部弹框

[返回目录](#目录)

#### Layout

- [vlayout](https://gitee.com/openharmony-tpc/vlayout) - 可以嵌套列表布局
- [flexbox-layout](https://gitee.com/openharmony-tpc/flexbox-layout) - 按照百分比控制的布局
- [ohosAutoLayout](https://gitee.com/openharmony-tpc/ohosAutoLayout) - 根据特定效果图尺寸，按比例自适应布局
- [yoga](https://gitee.com/openharmony-tpc/yoga) - facebook基于flexbox的布局引擎
- [TextLayoutBuilder](https://gitee.com/hihopeorg/TextLayoutBuilder) - Facebook的一款textlayout组件
- [FlowLayout](https://gitee.com/hihopeorg/FlowLayout) - 流式布局实现
- [ShadowLayout](https://gitee.com/chinasoft_ohos/ShadowLayout) - 带阴影效果的自定义layout
- [ExpandableLayout](https://gitee.com/chinasoft_ohos/ExpandableLayout) - 可折叠展开的layout
- [LayoutManagerGroup](https://gitee.com/openharmony-tpc/LayoutManagerGroup) - 负责测量和放置RecyclerView中的项目视图
- [Flipboard/bottomsheet](https://gitee.com/openharmony-tpc/bottomsheet) - 从屏幕底部显示可忽略的View
- [ohos-flowlayout](https://gitee.com/openharmony-tpc/ohos-flowlayout) - 流布局
- [ExpandableLayout](https://gitee.com/openharmony-tpc/ExpandableLayout) - 可动画扩展折叠子view布局

[返回目录](#目录)

#### Tab-菜单切换

- [FlycoTabLayout](https://gitee.com/openharmony-tpc/FlycoTabLayout) - 自定义TabLayout组件
- [NavigationTabBar](https://gitee.com/openharmony-tpc/NavigationTabBar) - 各种样式TabBar合集
- [BottomBar](https://gitee.com/openharmony-tpc/BottomBar) - 自定义底部菜单栏
- [BottomNavigation](https://gitee.com/openharmony-tpc/BottomNavigation) - 多种样式自定义底部菜单栏
- [ahbottomnavigation](https://gitee.com/openharmony-tpc/ahbottomnavigation) - 多种样式自定义底部菜单栏
- [HorizontalPicker](https://gitee.com/openharmony-tpc/HorizontalPicker) - 横向菜单选择器
- [StatefulLayout](https://gitee.com/openharmony-tpc/StatefulLayout) - 可以左右切换布局有点类似PageSlider，显示最常见的布局状态模板，如加载、空、错误布局等
- [NavigationTabStrip](https://gitee.com/hihopeorg/NavigationTabStrip) - Viewpager导航指示器,提供多种样式,支持自定义
- [PagerBottomTabStrip](https://gitee.com/hihopeorg/PagerBottomTabStrip) - 多种样式自定义底部和侧边的导航栏
- [SmartTabLayout](https://gitee.com/hihopeorg/SmartTabLayout) - 自定义TabLayout组件
- [XTabLayout](https://gitee.com/chinasoft_ohos/XTabLayout) - TabLayout的功能扩展
- [SHSegmentControl](https://gitee.com/chinasoft_ohos/SHSegmentControl) - 自定义菜单控件
- [BottomNavigationViewEx](https://gitee.com/openharmony-tpc/BottomNavigationViewEx) - 自定义底部导航栏

[返回目录](#目录)

#### Toasty-Toasty

- [Toasty](https://gitee.com/openharmony-tpc/Toasty) - 简单好用的Toast调用工具
- [FancyToast-ohos](https://gitee.com/openharmony-tpc/FancyToast-ohos) - Toast调用封装工具
- [TastyToast](https://gitee.com/hihopeorg/TastyToast) - 自定义Toast控件

[返回目录](#目录)

#### Time-Date

- [ohos-times-square](https://gitee.com/openharmony-tpc/ohos-times-square) - 简单的日历组件
- [CountdownView](https://gitee.com/openharmony-tpc/CountdownView) - 多种效果的时间计时器

[返回目录](#目录)

#### 其他UI-自定义控件

- [BGARefreshLayout-ohos](https://gitee.com/openharmony-tpc/BGARefreshLayout-ohos) - 基于多个场景的下拉刷新
- [ohos-Bootstrap](https://gitee.com/openharmony-tpc/ohos-Bootstrap) - 多种自定义控件合集
- [ohosSlidingUpPanel](https://gitee.com/openharmony-tpc/ohosSlidingUpPanel) - 底部上滑布局
- [Fragmentation](https://gitee.com/openharmony-tpc/Fragmentation) - 侧边菜单
- [triangle-view](https://gitee.com/openharmony-tpc/triangle-view) - 三角图
- [MaterialDesignLibrary](https://gitee.com/openharmony-tpc/MaterialDesignLibrary) - 基于MaterialDesign的各种自定义控件合集
- [XPopup](https://gitee.com/openharmony-tpc/XPopup) - 功能强大，交互优雅，动画丝滑的通用弹窗
- [cardslib](https://gitee.com/openharmony-tpc/cardslib) - 卡片式布局库
- [Swipecards](https://gitee.com/openharmony-tpc/Swipecards) - 滑动卡片组件
- [SlideUp-ohos](https://gitee.com/openharmony-tpc/SlideUp-ohos) - 从下方滑动出来的布局控件
- [EazeGraph](https://gitee.com/openharmony-tpc/EazeGraph) - 柱状图圆形图山峰图
- [WheelView](https://gitee.com/openharmony-tpc/WheelView) - 轮盘选择
- [RulerView](https://gitee.com/openharmony-tpc/RulerView) - 卷尺控件
- [MultiCardMenu](https://gitee.com/openharmony-tpc/MultiCardMenu) - 底部弹出的自定义菜单集合
- [DividerDrawable](https://gitee.com/openharmony-tpc/DividerDrawable) - 分割线绘制
- [ProtractorView](https://gitee.com/openharmony-tpc/ProtractorView) - 量角器控件
- [ohos-ExpandIcon](https://gitee.com/openharmony-tpc/ohos-ExpandIcon) - 箭头控件
- [GestureLock](https://gitee.com/openharmony-tpc/GestureLock) - 可自定义配置的手势动画解锁
- [williamchart](https://gitee.com/openharmony-tpc/williamchart) - 柱状图圆形图进度图山峰图
- [labelview](https://gitee.com/openharmony-tpc/labelview) - 自定义角标图
- [PatternLockView](https://gitee.com/openharmony-tpc/PatternLockView) - 简单的手势解锁
- [BadgeView](https://gitee.com/openharmony-tpc/BadgeView) - 图标的标签图
- [MaterialBadgeTextView](https://gitee.com/openharmony-tpc/MaterialBadgeTextView) - 自定义Text实现带有插入数字的彩色圆圈，该圆圈显示在图标的右上角，通常在IM应用程序中显示新消息或新功能的作用
- [SlantedTextView](https://gitee.com/openharmony-tpc/SlantedTextView) - 自定义角标图
- [TriangleLabelView](https://gitee.com/openharmony-tpc/TriangleLabelView) - 三角形角标图
- [GoodView](https://gitee.com/openharmony-tpc/GoodView) - 带特效点赞按钮
- [StateViews](https://gitee.com/openharmony-tpc/StateViews) - 自定义状态提示控件
- [WaveView](https://gitee.com/openharmony-tpc/WaveView) - 自定义水平面样式控件
- [CircleRefreshLayout](https://gitee.com/openharmony-tpc/CircleRefreshLayout) - 下拉刷新组件
- [TextDrawable](https://gitee.com/openharmony-tpc/TextDrawable) - 带有字母/文字的drawable
- [TextDrawable](https://gitee.com/hihopeorg/TextDrawable) - 带有字母/文字的drawable
- [OhosMaterialViews](https://gitee.com/hihopeorg/Ohos-material-views) - Material风格控件
- [baseAdapter](https://gitee.com/hihopeorg/baseAdapter) - ListView,RecyclerView,GridView适配器
- [Materialize](https://gitee.com/hihopeorg/Materialize) - Materia Design风格的主题库
- [FastAdapter](https://gitee.com/hihopeorg/FastAdapter) - 快速简化适配器
- [GestureViews](https://gitee.com/hihopeorg/GestureViews) - 带有手势控制和位置动画的ImageView和FrameLayout
- [GroupedRecyclerViewAdapter](https://gitee.com/hihopeorg/GroupedRecyclerViewAdapter) - RecyclerView适配器
- [ImmersionBar](https://gitee.com/hihopeorg/ImmersionBar) - 沉浸式状态栏导航栏实现
- [material](https://gitee.com/hihopeorg/material) - Material风格的UI控件库
- [MaterialDateTimePicker](https://gitee.com/hihopeorg/MaterialDateTimePicker) - Material风格的时间选择器
- [material-design-icons](https://gitee.com/hihopeorg/material-design-icons) - 提供material-design-icons图片资源
- [PanelSwitchHelper](https://gitee.com/hihopeorg/PanelSwitchHelper) - 输入法与面板流畅切换
- [SwipeBackLayout](https://gitee.com/hihopeorg/SwipeBackLayout) - 帮助构建带有向后滑动手势的应用程序
- [SwipeRevealLayout](https://gitee.com/hihopeorg/SwipeRevealLayout) - 上下左右滑动布局
- [EasyFlipView](https://gitee.com/hihopeorg/EasyFlipView) - 可以设定反转动画的自定义控件
- [JKeyboardPanelSwitch](https://gitee.com/hihopeorg/JKeyboardPanelSwitch) - 键盘面板冲突 布局闪动处理方案
- [MarqueeViewLibrary](https://gitee.com/hihopeorg/MarqueeViewLibrary) - 一个方便使用和扩展的跑马灯库
- [nice-spinner](https://gitee.com/hihopeorg/nice-spinner) - 简单好用的下拉框组件
- [PullZoomView](https://gitee.com/hihopeorg/PullZoomView) - 支持下拉顶部图片放大
- [WaveView](https://gitee.com/hihopeorg/WaveView) - 水波纹动画
- [search](https://gitee.com/hihopeorg/Search) - Material Design风格的搜索组件
- [Ohos-hellocharts](https://gitee.com/hihopeorg/Ohos-hellocharts) - 各种表格数据统计UI控件
- [TicketView](https://gitee.com/hihopeorg/TicketView) - 类似于观影二维码的票据视图
- [Ohos-StepsView](https://gitee.com/hihopeorg/Ohos-StepsView) - 显示步骤执行的自定义控件
- [OXChart](https://gitee.com/hihopeorg/OXChart) - 自定义图表库
- [RoundCorners](https://gitee.com/chinasoft_ohos/RoundCorners) - 自定义圆角Image，圆角Text等
- [MarqueeView](https://gitee.com/chinasoft_ohos/MarqueeView) - 自定义跑马灯控件
- [Captcha](https://gitee.com/chinasoft_ohos/Captcha) - 图片滑块解锁控件
- [achartengine](https://gitee.com/chinasoft_ohos/achartengine) - 柱状图圆形图进度图山峰图
- [LeafChart](https://gitee.com/chinasoft_ohos/LeafChart) - 支持折现、柱状的图表库
- [MessageBubbleView](https://gitee.com/chinasoft_ohos/MessageBubbleView) - 仿QQ未读消息气泡，可拖动删除
- [SuperLike](https://gitee.com/chinasoft_ohos/SuperLike) - 表情点赞功能
- [ohos_maskable_layout](https://gitee.com/chinasoft_ohos/ohos_maskable_layout) - 自定义component遮罩动画
- [Lighter](https://gitee.com/chinasoft_ohos/Lighter) - Lighter是一个首次进入页面的按钮提示功能库
- [E-signature](https://gitee.com/archermind-ti/E-signature) - 电子签名控件，支持签名边缘裁剪,根据速度进行了插值改变宽度
- [RippleView](https://gitee.com/chinasoft_ohos/RippleView) - 点击波纹效果
- [StickyScrollView](https://gitee.com/chinasoft_ohos/StickyScrollView) - 支持多种样式的ScrollView控件
- [ToolLibs](https://gitee.com/archermind-ti/tool-libs_ohos) - 一个自定义布局和动画的工具包
- [PatternLockView](https://gitee.com/archermind-ti/pattern-lock-view_ohos) - 手势绘制解锁控件
- [FingerPaintView](https://gitee.com/baijuncheng-open-source/FingerPaintView) - 多种样式的画笔绘制
- [SlidingMenu_ohos](https://gitee.com/isrc_ohos/sliding-menu_ohos) - 滑动菜单
- [Ultra-Pull-To-Refresh_ohos](https://gitee.com/isrc_ohos/ultra-pull-to-refresh_ohos) - 通用下拉刷新组件
- [MPChart_ohos](https://gitee.com/isrc_ohos/mp-chart_ohos) - 图表绘制组件
- [lock-screen](https://gitee.com/openharmony-tpc/lock-screen) - 简单漂亮的锁屏库
- [Graphview](https://gitee.com/openharmony-tpc/Graphview) - 图表库
- [Gloading](https://gitee.com/openharmony-tpc/Gloading) - 将应用中全局的Loading控件与页面解耦，默认提供5种加载状态（加载中、加载失败、空数据、加载成功，无网络），支持自定义其它状态
- [TimetableView](https://gitee.com/openharmony-tpc/TimetableView) - 一款开源、完善、高效的课程表控件，支持添加广告、课程重叠自动处理、透明背景设置、空白格子点击事件处理等丰富的功能
- [ohos-shapeLoadingView](https://gitee.com/openharmony-tpc/ohos-shapeLoadingView) - 仿58同城的Loading控件和Loading弹窗
- [polygonsview](https://gitee.com/openharmony-tpc/polygonsview) - 五边形蜘蛛网百分比库
- [MultipleStatusView](https://gitee.com/openharmony-tpc/MultipleStatusView) - 一个支持多种状态的自定义View,可以方便的切换到：加载中视图、错误视图、空数据视图、网络异常视图、内容视图
- [SlideshowToolbar](https://gitee.com/openharmony-tpc/SlideshowToolbar) - 使用slideshowimageview的幻灯片工具栏
- [ShowcaseView](https://gitee.com/openharmony-tpc/ShowcaseView) - 引导页
- [SlidingLayout](https://gitee.com/openharmony-tpc/SlidingLayout) - 下拉上拉弹跳的果冻效果
- [AnimatedCircleLoadingView](https://gitee.com/openharmony-tpc/AnimatedCircleLoadingView) - 确定/不确定的加载视图动画
- [SwipeBack](https://gitee.com/openharmony-tpc/SwipeBack) - 手势关闭页面
- [DiscreteSlider](https://gitee.com/openharmony-tpc/DiscreteSlider) - 自定义标签滑块
- [CustomWaterView](https://gitee.com/openharmony-tpc/CustomWaterView) - 自定义仿支付宝蚂蚁森林能量控件
- [WheelPicker](https://gitee.com/openharmony-tpc/WheelPicker) - 滚轮选择器
- [EasySwipeMenuLayout](https://gitee.com/openharmony-tpc/EasySwipeMenuLayout) - 滑动菜单库
- [floatingsearchview](https://gitee.com/openharmony-tpc/floatingsearchview) - 浮动搜索View
- [FlycoRoundView](https://gitee.com/openharmony-tpc/FlycoRoundView) - 设置圆形矩形背景
- [Ratingbar](https://gitee.com/openharmony-tpc/Ratingbar) - 自定义星级/等级
- [ohos-validation-komensky](https://gitee.com/openharmony-tpc/ohos-validation-komensky) - 使用批注验证表单中的用户输入
- [SystemBarTint](https://gitee.com/openharmony-tpc/SystemBarTint) - 将背景色应用于系统
- [Leonids](https://gitee.com/openharmony-tpc/Leonids) - 粒子效果库
- [CircleView](https://gitee.com/openharmony-tpc/CircleView) - 包含标题和副标题的圆形View
- [PercentageChartView](https://gitee.com/openharmony-tpc/PercentageChartView) - 自定义百分比ChartView
- [DatePicker](https://gitee.com/openharmony-tpc/DatePicker) - 日期选择器
- [SwipeCardView](https://gitee.com/openharmony-tpc/SwipeCardView) - 自定义滑动操作卡片
- [ValueCounter](https://gitee.com/openharmony-tpc/ValueCounter) - 自定义组件计数器
- [MyLittleCanvas](https://gitee.com/openharmony-tpc/MyLittleCanvas) - 辅助作画工具自定义控件集合
- [DragScaleCircleView](https://gitee.com/openharmony-tpc/DragScaleCircleView) - 可拖拽自定义圆形View
- [CircularFillableLoaders](https://gitee.com/openharmony-tpc/CircularFillableLoaders) - 水波纹浸漫式LoadingView
- [SpinMenu](https://gitee.com/openharmony-tpc/SpinMenu) - 轮盘式菜单选择控件
- [BubbleLayout](https://gitee.com/openharmony-tpc/BubbleLayout) - 自定义气泡组件
- [ohos-slidr](https://gitee.com/openharmony-tpc/ohos-slidr) - 自定义滑动条
- [ohos-SwitchView](https://gitee.com/openharmony-tpc/ohos-SwitchView) - 自定义开关按钮
- [material-intro-screen](https://gitee.com/openharmony-tpc/material-intro-screen) - Material设计滑动介绍页面

[返回目录](#目录)

### 框架类

#### 框架类

- [TheMVP](https://gitee.com/openharmony-tpc/TheMVP) - mvp框架
- [ohos-ZBLibrary](https://gitee.com/openharmony-tpc/ohos-ZBLibrary) - MVP框架，同时附有OKhttp，glide，zxing等常用工具
- [AutoDispose](https://gitee.com/hihopeorg/AutoDispose) - 基于RxJava进行自动绑定代码流式处理
- [mosby](https://gitee.com/hihopeorg/mosby) - 开源mvi、mvp模式适配项目
- [UpdatePlugin](https://gitee.com/hihopeorg/UpdatePlugin) - 一款用来进行app更新升级的框架

[返回目录](#目录)

### 动画图形类

#### 动画

- [ohosViewAnimations](https://gitee.com/openharmony-tpc/ohosViewAnimations) - 一款动画集合的库
- [lottie-ohos](https://gitee.com/openharmony-tpc/lottie-ohos) - json格式的动画解析渲染库
- [confetti](https://gitee.com/openharmony-tpc/confetti) - 模仿雪花飘落的动画
- [RippleEffect](https://gitee.com/openharmony-tpc/RippleEffect) - 水波纹点击动画
- [MetaballLoading](https://gitee.com/openharmony-tpc/MetaballLoading) -一个类似圆球进度动画效果
- [ohos-Spinkit](https://gitee.com/openharmony-tpc/ohos-Spinkit) -多种基础动画集合
- [LoadingView](https://gitee.com/openharmony-tpc/LoadingView) -21种简单的带有动画效果的加载控件
- [LoadingView另外一个版本](https://gitee.com/hihopeorg/LoadingView) -多种多样的loading动画集合
- [desertplaceholder](https://gitee.com/openharmony-tpc/desertplaceholder) -沙漠风格的动画占位页
- [Sequent](https://gitee.com/openharmony-tpc/Sequent) -为一个页面中的所有子控件提供动画效果，使页面更生动
- [ohos-Views](  https://gitee.com/openharmony-tpc/ohos-Views  ) -包含粒子效果，脉冲button效果，progress效果，底部导航栏等自定义组件的集合
- [BezierMaker](https://gitee.com/openharmony-tpc/BezierMaker) -简单的贝赛尔曲线绘制
- [WhorlView](https://gitee.com/openharmony-tpc/WhorlView) -圆形转圈动画
- [ohos-transition](https://gitee.com/hihopeorg/Ohos-transition) -平移动画库
- [Konfetti](https://gitee.com/hihopeorg/Konfetti) -纸屑粒子效果动画
- [LoadingDrawable](https://gitee.com/hihopeorg/LoadingDrawable) -提供16种加载动画， 适用于下拉刷新、图片加载的占位符、以及其他耗时操作场景
- [recyclerview-animators](https://gitee.com/hihopeorg/recyclerview-animators) -实现Item增加和删除的动画效果
- [ViewAnimator](https://gitee.com/hihopeorg/ViewAnimator) -多种布局的动画集合
- [Ohos-spruce](https://gitee.com/hihopeorg/Ohos-spruce) -轻量级平移转场动画
- [CanAnimation](https://gitee.com/chinasoft_ohos/CanAnimation) -使用ohos的属性动画写的一个库，可组建动画队列，可实现同时、顺序、重复播放等
- [LikeStarAnimation](https://gitee.com/chinasoft_ohos/LikeStarAnimation) -实现直播的点赞飘星效果
- [easing-interpolator_ohos](https://gitee.com/archermind-ti/easing-interpolator_ohos) -多种动画插值器轨迹展示
- [ohos-svprogress-hud-master](https://gitee.com/baijuncheng-open-source/ohos-svprogress-hud-master) -一个精仿ios提示的弹窗提示库，包括加载动画，失败与成功提示等
- [circular-anim](https://gitee.com/baijuncheng-open-source/circular-anim) -圆形转场动画
- [AnimatorValueLoadingIndicatorView_ohos](https://gitee.com/isrc_ohos/avloading-indicator-view_ohos) -支持加载动画的开关和隐藏，支持多种加载动画效果
- [AZExplosion](https://gitee.com/isrc_ohos/azexplosion_ohos) -粒子破碎效果
- [SwipeCaptcha_ohos](https://gitee.com/isrc_ohos/swipe-captcha_ohos) -滑动验证码
- [ContinuousScrollableImageView_ohos](https://gitee.com/isrc_ohos/continuous-scrollable-image-view_ohos) -连续滚动图像控件，可有效显示具有连续滚动效果的图像
- [DanmakuFlameMaster_ohos](https://gitee.com/isrc_ohos/danmaku-flame-master_ohos) -弹幕解析绘制
- [Transitions-Everywhere](https://gitee.com/openharmony-tpc/Transitions-Everywhere) -转场动画
- [AnimationEasingFunctions](https://gitee.com/openharmony-tpc/AnimationEasingFunctions) -多种估值器动画运动轨迹的集合
- [MultiWaveHeader](https://gitee.com/openharmony-tpc/MultiWaveHeader) -自定义水波控件
- [ohos-animated-menu-items](https://gitee.com/openharmony-tpc/ohos-animated-menu-items) -自定义动画菜单条目小控件

[返回目录](#目录)

#### 图片处理

- [SimpleCropView](https://gitee.com/openharmony-tpc/SimpleCropView) - 图片裁剪工具
- [Luban](https://gitee.com/openharmony-tpc/Luban) - 图片压缩工具
- [TakePhoto](https://gitee.com/openharmony-tpc/TakePhoto) - 拍照图片旋转剪裁
- [Compressor](https://gitee.com/openharmony-tpc/Compressor) - 一个轻量级且功能强大的图像压缩库。通过Compressor，您可以将大照片压缩为较小尺寸的照片，而图像质量的损失则很小或可以忽略不计，不支持WebP
- [PloyFun](https://gitee.com/openharmony-tpc/PloyFun) - 生成三角玻璃图片
- [CompressHelper](https://gitee.com/openharmony-tpc/CompressHelper) - 图片压缩，压缩Pixelmap，主要通过尺寸压缩和质量压缩，以达到清晰度最优
- [compresshelper-master](https://gitee.com/baijuncheng-open-source/compresshelper-master) - 图片压缩库
- [SimpleCropView](https://gitee.com/hihopeorg/SimpleCropView) - 图片裁剪工具
- [cropper](https://gitee.com/hihopeorg/cropper) - 图像裁剪工具
- [cropper2](https://gitee.com/openharmony-tpc/cropper) - 图片裁剪
- [boxing](https://gitee.com/hihopeorg/boxing) - 支持图片旋转裁剪多图选择等功能
- [Ohos-stackblur](https://gitee.com/hihopeorg/Ohos-stackblur) - 图片模糊效果
- [ImageCropper_ohos](https://gitee.com/isrc_ohos/image-cropper_ohos) - 图片裁剪
- [uCrop_ohos](https://gitee.com/isrc_ohos/u-crop_ohos) - 图像裁剪
- [ Crop_ohos](https://gitee.com/isrc_ohos/crop_ohos) - 图片裁剪
- [crop_image_layout_ohos](https://gitee.com/isrc_ohos/crop_image_layout_ohos) - 图片裁剪
- [Lichenwei-Dev_ImagePicker](https://gitee.com/openharmony-tpc/Lichenwei-Dev_ImagePicker) - 图片选择预览加载器

[返回目录](#目录)

### 音视频

- [jcodec java](https://gitee.com/openharmony-tpc/jcodec) - 纯java实现的音视频编解码器的库
- [VideoCache_ohos](https://gitee.com/isrc_ohos/video-cache_ohos) - 开源视频缓存项目，支持自动缓存视频并在断网状态下播放视频
- [soundtouch](https://gitee.com/openharmony-tpc/soundtouch) - 开源音频处理库，可更改音频流或音频文件的速度、音高和播放速率
- [ohosMP3Recorder](https://gitee.com/openharmony-tpc/ohosMP3Recorder) - 提供MP3录音功能
- [ijkplayer](https://gitee.com/openharmony-tpc/ijkplayer) -基于FFmpeg的ohos视频播放器，除了常规的播放器功能外，多用于直播流场景，支持常见的各种流媒体协议和音视频格式

[返回目录](#目录)